﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QuanLyThuPhi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPhiMaDG = New System.Windows.Forms.TextBox()
        Me.txtPhiTenDG = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtPhiNgayDong = New System.Windows.Forms.DateTimePicker()
        Me.dtPhiNgayHet = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblPhiTrangThai = New System.Windows.Forms.Label()
        Me.btnPhiCapNhat = New System.Windows.Forms.Button()
        Me.btnPhiIn = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPhiTenDG)
        Me.GroupBox1.Controls.Add(Me.txtPhiMaDG)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(307, 91)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thông tin độc giả"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Mã độc giả"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tên độc giả"
        '
        'txtPhiMaDG
        '
        Me.txtPhiMaDG.Enabled = False
        Me.txtPhiMaDG.Location = New System.Drawing.Point(97, 27)
        Me.txtPhiMaDG.Name = "txtPhiMaDG"
        Me.txtPhiMaDG.Size = New System.Drawing.Size(115, 21)
        Me.txtPhiMaDG.TabIndex = 2
        '
        'txtPhiTenDG
        '
        Me.txtPhiTenDG.Enabled = False
        Me.txtPhiTenDG.Location = New System.Drawing.Point(97, 52)
        Me.txtPhiTenDG.Name = "txtPhiTenDG"
        Me.txtPhiTenDG.Size = New System.Drawing.Size(204, 21)
        Me.txtPhiTenDG.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPhiTrangThai)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.dtPhiNgayHet)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.dtPhiNgayDong)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(14, 111)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(307, 115)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Thông tin thu phí"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Ngày đóng"
        '
        'dtPhiNgayDong
        '
        Me.dtPhiNgayDong.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPhiNgayDong.Location = New System.Drawing.Point(97, 21)
        Me.dtPhiNgayDong.Name = "dtPhiNgayDong"
        Me.dtPhiNgayDong.Size = New System.Drawing.Size(204, 21)
        Me.dtPhiNgayDong.TabIndex = 1
        '
        'dtPhiNgayHet
        '
        Me.dtPhiNgayHet.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPhiNgayHet.Location = New System.Drawing.Point(97, 50)
        Me.dtPhiNgayHet.Name = "dtPhiNgayHet"
        Me.dtPhiNgayHet.Size = New System.Drawing.Size(203, 21)
        Me.dtPhiNgayHet.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(79, 15)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Ngày hết hạn"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Trạng thái"
        '
        'lblPhiTrangThai
        '
        Me.lblPhiTrangThai.AutoSize = True
        Me.lblPhiTrangThai.ForeColor = System.Drawing.Color.Red
        Me.lblPhiTrangThai.Location = New System.Drawing.Point(94, 82)
        Me.lblPhiTrangThai.Name = "lblPhiTrangThai"
        Me.lblPhiTrangThai.Size = New System.Drawing.Size(75, 15)
        Me.lblPhiTrangThai.TabIndex = 5
        Me.lblPhiTrangThai.Text = "Còn 12 ngày"
        '
        'btnPhiCapNhat
        '
        Me.btnPhiCapNhat.Location = New System.Drawing.Point(236, 232)
        Me.btnPhiCapNhat.Name = "btnPhiCapNhat"
        Me.btnPhiCapNhat.Size = New System.Drawing.Size(79, 30)
        Me.btnPhiCapNhat.TabIndex = 2
        Me.btnPhiCapNhat.Text = "Cập nhật"
        Me.btnPhiCapNhat.UseVisualStyleBackColor = True
        '
        'btnPhiIn
        '
        Me.btnPhiIn.Location = New System.Drawing.Point(151, 232)
        Me.btnPhiIn.Name = "btnPhiIn"
        Me.btnPhiIn.Size = New System.Drawing.Size(79, 30)
        Me.btnPhiIn.TabIndex = 3
        Me.btnPhiIn.Text = "In phiếu"
        Me.btnPhiIn.UseVisualStyleBackColor = True
        '
        'QuanLyThuPhi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(333, 265)
        Me.Controls.Add(Me.btnPhiIn)
        Me.Controls.Add(Me.btnPhiCapNhat)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "QuanLyThuPhi"
        Me.Text = "Quản lý thu phí"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPhiTenDG As System.Windows.Forms.TextBox
    Friend WithEvents txtPhiMaDG As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dtPhiNgayHet As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtPhiNgayDong As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblPhiTrangThai As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnPhiCapNhat As System.Windows.Forms.Button
    Friend WithEvents btnPhiIn As System.Windows.Forms.Button
End Class
