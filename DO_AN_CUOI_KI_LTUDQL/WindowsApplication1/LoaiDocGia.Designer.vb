﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoaiDocGia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvListDG = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMaLoaiDG = New System.Windows.Forms.TextBox()
        Me.txtTenLoaiDG = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPhiThuongNien = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtSoSachMuon = New System.Windows.Forms.NumericUpDown()
        Me.txtSoNgayMuon = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtMoTaLoaiDG = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnSua = New System.Windows.Forms.Button()
        Me.btnXoa = New System.Windows.Forms.Button()
        Me.btnThem = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvListDG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSoSachMuon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSoNgayMuon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtMoTaLoaiDG)
        Me.GroupBox1.Controls.Add(Me.txtSoNgayMuon)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtSoSachMuon)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtPhiThuongNien)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtTenLoaiDG)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtMaLoaiDG)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(549, 167)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thông tin"
        '
        'dgvListDG
        '
        Me.dgvListDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListDG.Location = New System.Drawing.Point(12, 219)
        Me.dgvListDG.Name = "dgvListDG"
        Me.dgvListDG.Size = New System.Drawing.Size(549, 227)
        Me.dgvListDG.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Mã "
        '
        'txtMaLoaiDG
        '
        Me.txtMaLoaiDG.Enabled = False
        Me.txtMaLoaiDG.Location = New System.Drawing.Point(121, 24)
        Me.txtMaLoaiDG.Name = "txtMaLoaiDG"
        Me.txtMaLoaiDG.Size = New System.Drawing.Size(89, 21)
        Me.txtMaLoaiDG.TabIndex = 1
        '
        'txtTenLoaiDG
        '
        Me.txtTenLoaiDG.Location = New System.Drawing.Point(121, 50)
        Me.txtTenLoaiDG.Name = "txtTenLoaiDG"
        Me.txtTenLoaiDG.Size = New System.Drawing.Size(168, 21)
        Me.txtTenLoaiDG.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tên"
        '
        'txtPhiThuongNien
        '
        Me.txtPhiThuongNien.Location = New System.Drawing.Point(121, 76)
        Me.txtPhiThuongNien.Name = "txtPhiThuongNien"
        Me.txtPhiThuongNien.Size = New System.Drawing.Size(168, 21)
        Me.txtPhiThuongNien.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Phí thường niên"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 15)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Mượn tối đa"
        '
        'txtSoSachMuon
        '
        Me.txtSoSachMuon.Location = New System.Drawing.Point(121, 104)
        Me.txtSoSachMuon.Name = "txtSoSachMuon"
        Me.txtSoSachMuon.Size = New System.Drawing.Size(168, 21)
        Me.txtSoSachMuon.TabIndex = 7
        Me.txtSoSachMuon.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtSoNgayMuon
        '
        Me.txtSoNgayMuon.Location = New System.Drawing.Point(121, 131)
        Me.txtSoNgayMuon.Name = "txtSoNgayMuon"
        Me.txtSoNgayMuon.Size = New System.Drawing.Size(168, 21)
        Me.txtSoNgayMuon.TabIndex = 10
        Me.txtSoNgayMuon.Value = New Decimal(New Integer() {15, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 133)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 15)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Số ngày mượn"
        '
        'txtMoTaLoaiDG
        '
        Me.txtMoTaLoaiDG.Location = New System.Drawing.Point(306, 47)
        Me.txtMoTaLoaiDG.Multiline = True
        Me.txtMoTaLoaiDG.Name = "txtMoTaLoaiDG"
        Me.txtMoTaLoaiDG.Size = New System.Drawing.Size(237, 105)
        Me.txtMoTaLoaiDG.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(303, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 15)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Mô tả"
        '
        'btnSua
        '
        Me.btnSua.Enabled = False
        Me.btnSua.Location = New System.Drawing.Point(93, 190)
        Me.btnSua.Name = "btnSua"
        Me.btnSua.Size = New System.Drawing.Size(75, 23)
        Me.btnSua.TabIndex = 2
        Me.btnSua.Text = "Cập nhật"
        Me.btnSua.UseVisualStyleBackColor = True
        '
        'btnXoa
        '
        Me.btnXoa.Enabled = False
        Me.btnXoa.Location = New System.Drawing.Point(174, 190)
        Me.btnXoa.Name = "btnXoa"
        Me.btnXoa.Size = New System.Drawing.Size(75, 23)
        Me.btnXoa.TabIndex = 3
        Me.btnXoa.Text = "Xóa"
        Me.btnXoa.UseVisualStyleBackColor = True
        '
        'btnThem
        '
        Me.btnThem.Location = New System.Drawing.Point(480, 185)
        Me.btnThem.Name = "btnThem"
        Me.btnThem.Size = New System.Drawing.Size(75, 23)
        Me.btnThem.TabIndex = 4
        Me.btnThem.Text = "Thêm"
        Me.btnThem.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(12, 190)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnRefresh.TabIndex = 5
        Me.btnRefresh.Text = "Tải dữ liệu"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'LoaiDocGia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(572, 458)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnThem)
        Me.Controls.Add(Me.btnXoa)
        Me.Controls.Add(Me.btnSua)
        Me.Controls.Add(Me.dgvListDG)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "LoaiDocGia"
        Me.Text = "Quản lý loại độc giả"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvListDG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSoSachMuon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSoNgayMuon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvListDG As System.Windows.Forms.DataGridView
    Friend WithEvents txtSoNgayMuon As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtSoSachMuon As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPhiThuongNien As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTenLoaiDG As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtMaLoaiDG As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtMoTaLoaiDG As System.Windows.Forms.TextBox
    Friend WithEvents btnSua As System.Windows.Forms.Button
    Friend WithEvents btnXoa As System.Windows.Forms.Button
    Friend WithEvents btnThem As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
End Class
