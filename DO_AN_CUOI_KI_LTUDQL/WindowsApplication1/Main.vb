﻿Public Class frmMain

    'hien thi form gioi thieu
    Private Sub mnuGioiThieu_Click(sender As Object, e As EventArgs) Handles mnuGioiThieu.Click
        Dim frmAbout As New About()
        frmAbout.ShowDialog()
    End Sub
    'su ly nut dang xuất
    Private Sub picDangXuat_Click(sender As Object, e As EventArgs) Handles picDangXuat.Click
        Dim confirmExit As Integer = MessageBox.Show("Xác nhận thoát", "Thoát ứng dụng", MessageBoxButtons.YesNo
                                                     )
        If confirmExit = DialogResult.Yes Then
            Me.Close()
        End If
    End Sub

    'Su ly nut btnShortLoaiDocgia
    Private Sub btnShortLoaiDocGia_Click(sender As Object, e As EventArgs) Handles btnShortLoaiDocGia.Click
        Dim frmLoaiDocgia As New LoaiDocGia()
        frmLoaiDocgia.Show()
    End Sub

    'Nut hien thi form dang ki doc gia
    Private Sub btnShortDangKiDG_Click(sender As Object, e As EventArgs) Handles btnShortDangKiDG.Click
        Dim frmDangKiDocGia As New frmDangKiDocGia()
        frmDangKiDocGia.Show()
    End Sub

    'hien thi form tim kiem /form quản lý doc gia
    Private Sub btnShortTimDG_Click(sender As Object, e As EventArgs) Handles btnShortTimDG.Click

        Dim frmQuanLyDocGia As New DocGia()
        frmQuanLyDocGia.Show()
    End Sub
End Class
