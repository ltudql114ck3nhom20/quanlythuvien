﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.HệThốngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuCaiDat = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportDữLiệuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuThoat = New System.Windows.Forms.ToolStripMenuItem()
        Me.TaifToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuDanhMuc = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuTaiLieu = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuThemTaiLieu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ĐộcGiảToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuLoaiDG = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuDG = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuThuPhi = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuIn = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuMuonSach = New System.Windows.Forms.ToolStripMenuItem()
        Me.MượnSáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuTraSach = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuPhiPhatSinh = New System.Windows.Forms.ToolStripMenuItem()
        Me.NgườiDùngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuTaikhoan = New System.Windows.Forms.ToolStripMenuItem()
        Me.submnuThemTaiKhoan = New System.Windows.Forms.ToolStripMenuItem()
        Me.ĐăngXuấtToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGioiThieu = New System.Windows.Forms.ToolStripMenuItem()
        Me.sttMain = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnTimNhanh = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.optPhieuTra = New System.Windows.Forms.RadioButton()
        Me.optPhieuMuon = New System.Windows.Forms.RadioButton()
        Me.optDocGia = New System.Windows.Forms.RadioButton()
        Me.optTaiLieu = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtTimNhanh = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.picDangXuat = New System.Windows.Forms.PictureBox()
        Me.picThongTinTaiKhoan = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnShortTheoDoiTL = New System.Windows.Forms.Button()
        Me.btnShortLapPhieuTra = New System.Windows.Forms.Button()
        Me.btnShortLapPhieuMuon = New System.Windows.Forms.Button()
        Me.btnShortLoaiDocGia = New System.Windows.Forms.Button()
        Me.btnShortDangKiDG = New System.Windows.Forms.Button()
        Me.btnShortTimDG = New System.Windows.Forms.Button()
        Me.btnShortcutTimTaiLieu = New System.Windows.Forms.Button()
        Me.btnShortSuaTaiLieu = New System.Windows.Forms.Button()
        Me.btnShortThemTaiLieu = New System.Windows.Forms.Button()
        Me.btnShortThemDanhMuc = New System.Windows.Forms.Button()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.mnuMain.SuspendLayout()
        Me.sttMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.picDangXuat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picThongTinTaiKhoan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuMain
        '
        Me.mnuMain.BackColor = System.Drawing.SystemColors.MenuBar
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HệThốngToolStripMenuItem, Me.TaifToolStripMenuItem, Me.ĐộcGiảToolStripMenuItem, Me.submnuMuonSach, Me.NgườiDùngToolStripMenuItem, Me.mnuGioiThieu})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(910, 24)
        Me.mnuMain.TabIndex = 0
        Me.mnuMain.Text = "MenuStrip1"
        '
        'HệThốngToolStripMenuItem
        '
        Me.HệThốngToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.submnuCaiDat, Me.BackupToolStripMenuItem, Me.ImportDữLiệuToolStripMenuItem, Me.submnuThoat})
        Me.HệThốngToolStripMenuItem.Name = "HệThốngToolStripMenuItem"
        Me.HệThốngToolStripMenuItem.Size = New System.Drawing.Size(69, 20)
        Me.HệThốngToolStripMenuItem.Text = "Hệ thống"
        '
        'submnuCaiDat
        '
        Me.submnuCaiDat.Name = "submnuCaiDat"
        Me.submnuCaiDat.Size = New System.Drawing.Size(152, 22)
        Me.submnuCaiDat.Text = "Cài đặt"
        '
        'BackupToolStripMenuItem
        '
        Me.BackupToolStripMenuItem.Name = "BackupToolStripMenuItem"
        Me.BackupToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BackupToolStripMenuItem.Text = "Backup dữ liệu"
        '
        'ImportDữLiệuToolStripMenuItem
        '
        Me.ImportDữLiệuToolStripMenuItem.Name = "ImportDữLiệuToolStripMenuItem"
        Me.ImportDữLiệuToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ImportDữLiệuToolStripMenuItem.Text = "Import dữ liệu"
        '
        'submnuThoat
        '
        Me.submnuThoat.Name = "submnuThoat"
        Me.submnuThoat.Size = New System.Drawing.Size(152, 22)
        Me.submnuThoat.Text = "Thoát"
        '
        'TaifToolStripMenuItem
        '
        Me.TaifToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.submnuDanhMuc, Me.submnuTaiLieu, Me.submnuThemTaiLieu})
        Me.TaifToolStripMenuItem.Name = "TaifToolStripMenuItem"
        Me.TaifToolStripMenuItem.Size = New System.Drawing.Size(56, 20)
        Me.TaifToolStripMenuItem.Text = "&Tài liệu"
        '
        'submnuDanhMuc
        '
        Me.submnuDanhMuc.Name = "submnuDanhMuc"
        Me.submnuDanhMuc.Size = New System.Drawing.Size(173, 22)
        Me.submnuDanhMuc.Text = "Quản lý Danh mục"
        '
        'submnuTaiLieu
        '
        Me.submnuTaiLieu.Name = "submnuTaiLieu"
        Me.submnuTaiLieu.Size = New System.Drawing.Size(173, 22)
        Me.submnuTaiLieu.Text = "Quản lý tài liệu"
        '
        'submnuThemTaiLieu
        '
        Me.submnuThemTaiLieu.Name = "submnuThemTaiLieu"
        Me.submnuThemTaiLieu.Size = New System.Drawing.Size(173, 22)
        Me.submnuThemTaiLieu.Text = "Thêm tài liệu"
        '
        'ĐộcGiảToolStripMenuItem
        '
        Me.ĐộcGiảToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.submnuLoaiDG, Me.submnuDG, Me.submnuThuPhi, Me.submnuIn})
        Me.ĐộcGiảToolStripMenuItem.Name = "ĐộcGiảToolStripMenuItem"
        Me.ĐộcGiảToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.ĐộcGiảToolStripMenuItem.Text = "&Độc giả"
        '
        'submnuLoaiDG
        '
        Me.submnuLoaiDG.Name = "submnuLoaiDG"
        Me.submnuLoaiDG.Size = New System.Drawing.Size(183, 22)
        Me.submnuLoaiDG.Text = "Loại độc giả"
        '
        'submnuDG
        '
        Me.submnuDG.Name = "submnuDG"
        Me.submnuDG.Size = New System.Drawing.Size(183, 22)
        Me.submnuDG.Text = "Quản lý độc giả"
        '
        'submnuThuPhi
        '
        Me.submnuThuPhi.Name = "submnuThuPhi"
        Me.submnuThuPhi.Size = New System.Drawing.Size(183, 22)
        Me.submnuThuPhi.Text = "Thu phí thường niên"
        '
        'submnuIn
        '
        Me.submnuIn.Name = "submnuIn"
        Me.submnuIn.Size = New System.Drawing.Size(183, 22)
        Me.submnuIn.Text = "In thông tin"
        '
        'submnuMuonSach
        '
        Me.submnuMuonSach.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MượnSáchToolStripMenuItem, Me.submnuTraSach, Me.submnuPhiPhatSinh})
        Me.submnuMuonSach.Name = "submnuMuonSach"
        Me.submnuMuonSach.Size = New System.Drawing.Size(97, 20)
        Me.submnuMuonSach.Text = "&Mượn Trả sách"
        '
        'MượnSáchToolStripMenuItem
        '
        Me.MượnSáchToolStripMenuItem.Name = "MượnSáchToolStripMenuItem"
        Me.MượnSáchToolStripMenuItem.Size = New System.Drawing.Size(135, 22)
        Me.MượnSáchToolStripMenuItem.Text = "Mượn sách"
        '
        'submnuTraSach
        '
        Me.submnuTraSach.Name = "submnuTraSach"
        Me.submnuTraSach.Size = New System.Drawing.Size(135, 22)
        Me.submnuTraSach.Text = "Trả sách"
        '
        'submnuPhiPhatSinh
        '
        Me.submnuPhiPhatSinh.Name = "submnuPhiPhatSinh"
        Me.submnuPhiPhatSinh.Size = New System.Drawing.Size(135, 22)
        Me.submnuPhiPhatSinh.Text = "Quản lý phí"
        '
        'NgườiDùngToolStripMenuItem
        '
        Me.NgườiDùngToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.submnuTaikhoan, Me.submnuThemTaiKhoan, Me.ĐăngXuấtToolStripMenuItem})
        Me.NgườiDùngToolStripMenuItem.Name = "NgườiDùngToolStripMenuItem"
        Me.NgườiDùngToolStripMenuItem.Size = New System.Drawing.Size(83, 20)
        Me.NgườiDùngToolStripMenuItem.Text = "&Người dùng"
        '
        'submnuTaikhoan
        '
        Me.submnuTaikhoan.Name = "submnuTaikhoan"
        Me.submnuTaikhoan.Size = New System.Drawing.Size(150, 22)
        Me.submnuTaikhoan.Text = "Xem thông tin"
        '
        'submnuThemTaiKhoan
        '
        Me.submnuThemTaiKhoan.Name = "submnuThemTaiKhoan"
        Me.submnuThemTaiKhoan.Size = New System.Drawing.Size(150, 22)
        Me.submnuThemTaiKhoan.Text = "Thêm quản trị"
        '
        'ĐăngXuấtToolStripMenuItem
        '
        Me.ĐăngXuấtToolStripMenuItem.Name = "ĐăngXuấtToolStripMenuItem"
        Me.ĐăngXuấtToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.ĐăngXuấtToolStripMenuItem.Text = "Đăng xuất"
        '
        'mnuGioiThieu
        '
        Me.mnuGioiThieu.Name = "mnuGioiThieu"
        Me.mnuGioiThieu.Size = New System.Drawing.Size(70, 20)
        Me.mnuGioiThieu.Text = "&Giới thiệu"
        '
        'sttMain
        '
        Me.sttMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2, Me.ToolStripSplitButton1, Me.ToolStripStatusLabel3})
        Me.sttMain.Location = New System.Drawing.Point(0, 533)
        Me.sttMain.Name = "sttMain"
        Me.sttMain.Size = New System.Drawing.Size(910, 22)
        Me.sttMain.TabIndex = 2
        Me.sttMain.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Ready"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(90, 17)
        Me.ToolStripStatusLabel3.Text = "Tài khoản: qhxh"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Location = New System.Drawing.Point(12, 27)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(214, 503)
        Me.Panel1.TabIndex = 3
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnShortTheoDoiTL)
        Me.GroupBox3.Controls.Add(Me.btnShortLapPhieuTra)
        Me.GroupBox3.Controls.Add(Me.btnShortLapPhieuMuon)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 365)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(204, 131)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Mượn/Trả tài liệu"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnShortLoaiDocGia)
        Me.GroupBox2.Controls.Add(Me.btnShortDangKiDG)
        Me.GroupBox2.Controls.Add(Me.btnShortTimDG)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 190)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(204, 158)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Độc giả"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnShortcutTimTaiLieu)
        Me.GroupBox1.Controls.Add(Me.btnShortSuaTaiLieu)
        Me.GroupBox1.Controls.Add(Me.btnShortThemTaiLieu)
        Me.GroupBox1.Controls.Add(Me.btnShortThemDanhMuc)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(204, 170)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tài liệu"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.Info
        Me.GroupBox4.Controls.Add(Me.Label24)
        Me.GroupBox4.Controls.Add(Me.Label23)
        Me.GroupBox4.Controls.Add(Me.Label22)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.PictureBox5)
        Me.GroupBox4.Controls.Add(Me.PictureBox4)
        Me.GroupBox4.Controls.Add(Me.PictureBox3)
        Me.GroupBox4.Controls.Add(Me.PictureBox2)
        Me.GroupBox4.Controls.Add(Me.PictureBox1)
        Me.GroupBox4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox4.Location = New System.Drawing.Point(232, 32)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(666, 314)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Thống kê"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(241, 271)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(61, 16)
        Me.Label24.TabIndex = 26
        Me.Label24.Text = "(độc giả)"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(241, 241)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(61, 16)
        Me.Label23.TabIndex = 25
        Me.Label23.Text = "(độc giả)"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(241, 210)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(54, 16)
        Me.Label22.TabIndex = 24
        Me.Label22.Text = "(tài liệu)"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(241, 181)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(54, 16)
        Me.Label21.TabIndex = 23
        Me.Label21.Text = "(tài liệu)"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Maroon
        Me.Label20.Location = New System.Drawing.Point(199, 271)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(24, 16)
        Me.Label20.TabIndex = 22
        Me.Label20.Text = "12"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Maroon
        Me.Label19.Location = New System.Drawing.Point(199, 241)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(24, 16)
        Me.Label19.TabIndex = 21
        Me.Label19.Text = "50"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Maroon
        Me.Label18.Location = New System.Drawing.Point(191, 210)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(32, 16)
        Me.Label18.TabIndex = 20
        Me.Label18.Text = "210"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Maroon
        Me.Label17.Location = New System.Drawing.Point(191, 181)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 16)
        Me.Label17.TabIndex = 19
        Me.Label17.Text = "554"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(34, 271)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(144, 16)
        Me.Label16.TabIndex = 18
        Me.Label16.Text = "Độc giả chưa đóng phí:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(34, 241)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(105, 16)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "Độc giả quá hạn"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(34, 210)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(106, 16)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Tài liệu quá hạn:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(34, 181)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(128, 16)
        Me.Label13.TabIndex = 15
        Me.Label13.Text = "Tài liệu đang mượn: "
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(589, 97)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 16)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "1622"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(458, 97)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 16)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "2001"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(324, 97)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 16)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "1020"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(187, 97)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 16)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "1024"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(64, 97)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(24, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "12"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label7.Location = New System.Drawing.Point(563, 140)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 16)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Phiếu mượn"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label6.Location = New System.Drawing.Point(431, 140)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Phiếu trả"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label5.Location = New System.Drawing.Point(303, 140)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Độc giả"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label4.Location = New System.Drawing.Point(163, 140)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Tài liệu"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(31, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Chuyên mục"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox5.Controls.Add(Me.btnTimNhanh)
        Me.GroupBox5.Controls.Add(Me.GroupBox7)
        Me.GroupBox5.Controls.Add(Me.Label2)
        Me.GroupBox5.Controls.Add(Me.txtTimNhanh)
        Me.GroupBox5.Controls.Add(Me.Label1)
        Me.GroupBox5.Location = New System.Drawing.Point(232, 352)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(572, 173)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Tìm kiếm nhanh"
        '
        'btnTimNhanh
        '
        Me.btnTimNhanh.Location = New System.Drawing.Point(269, 112)
        Me.btnTimNhanh.Name = "btnTimNhanh"
        Me.btnTimNhanh.Size = New System.Drawing.Size(75, 23)
        Me.btnTimNhanh.TabIndex = 4
        Me.btnTimNhanh.Text = "Tìm kiếm"
        Me.btnTimNhanh.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.optPhieuTra)
        Me.GroupBox7.Controls.Add(Me.optPhieuMuon)
        Me.GroupBox7.Controls.Add(Me.optDocGia)
        Me.GroupBox7.Controls.Add(Me.optTaiLieu)
        Me.GroupBox7.Location = New System.Drawing.Point(106, 64)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(442, 31)
        Me.GroupBox7.TabIndex = 3
        Me.GroupBox7.TabStop = False
        '
        'optPhieuTra
        '
        Me.optPhieuTra.AutoSize = True
        Me.optPhieuTra.Location = New System.Drawing.Point(355, 9)
        Me.optPhieuTra.Name = "optPhieuTra"
        Me.optPhieuTra.Size = New System.Drawing.Size(78, 20)
        Me.optPhieuTra.TabIndex = 3
        Me.optPhieuTra.Text = "Phiếu trả"
        Me.optPhieuTra.UseVisualStyleBackColor = True
        '
        'optPhieuMuon
        '
        Me.optPhieuMuon.AutoSize = True
        Me.optPhieuMuon.Location = New System.Drawing.Point(234, 9)
        Me.optPhieuMuon.Name = "optPhieuMuon"
        Me.optPhieuMuon.Size = New System.Drawing.Size(96, 20)
        Me.optPhieuMuon.TabIndex = 2
        Me.optPhieuMuon.Text = "Phiếu mượn"
        Me.optPhieuMuon.UseVisualStyleBackColor = True
        '
        'optDocGia
        '
        Me.optDocGia.AutoSize = True
        Me.optDocGia.Location = New System.Drawing.Point(117, 8)
        Me.optDocGia.Name = "optDocGia"
        Me.optDocGia.Size = New System.Drawing.Size(72, 20)
        Me.optDocGia.TabIndex = 1
        Me.optDocGia.Text = "Độc giả"
        Me.optDocGia.UseVisualStyleBackColor = True
        '
        'optTaiLieu
        '
        Me.optTaiLieu.AutoSize = True
        Me.optTaiLieu.Checked = True
        Me.optTaiLieu.Location = New System.Drawing.Point(6, 8)
        Me.optTaiLieu.Name = "optTaiLieu"
        Me.optTaiLieu.Size = New System.Drawing.Size(70, 20)
        Me.optTaiLieu.TabIndex = 0
        Me.optTaiLieu.TabStop = True
        Me.optTaiLieu.Text = "Tài liệu"
        Me.optTaiLieu.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Lọc theo"
        '
        'txtTimNhanh
        '
        Me.txtTimNhanh.Location = New System.Drawing.Point(106, 42)
        Me.txtTimNhanh.Name = "txtTimNhanh"
        Me.txtTimNhanh.Size = New System.Drawing.Size(442, 22)
        Me.txtTimNhanh.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Từ khóa"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label26)
        Me.GroupBox6.Controls.Add(Me.Label25)
        Me.GroupBox6.Controls.Add(Me.picDangXuat)
        Me.GroupBox6.Controls.Add(Me.picThongTinTaiKhoan)
        Me.GroupBox6.Location = New System.Drawing.Point(810, 352)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(88, 173)
        Me.GroupBox6.TabIndex = 7
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Tài khoản"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(27, 148)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(38, 15)
        Me.Label26.TabIndex = 3
        Me.Label26.Text = "Thoát"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(20, 70)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(58, 15)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Thông tin"
        '
        'picDangXuat
        '
        Me.picDangXuat.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picDangXuat.Image = Global.WindowsApplication1.My.Resources.Resources.Windows_Log_Off_icon
        Me.picDangXuat.Location = New System.Drawing.Point(23, 104)
        Me.picDangXuat.Name = "picDangXuat"
        Me.picDangXuat.Size = New System.Drawing.Size(49, 41)
        Me.picDangXuat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picDangXuat.TabIndex = 1
        Me.picDangXuat.TabStop = False
        '
        'picThongTinTaiKhoan
        '
        Me.picThongTinTaiKhoan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picThongTinTaiKhoan.Image = Global.WindowsApplication1.My.Resources.Resources.Admin_icon
        Me.picThongTinTaiKhoan.Location = New System.Drawing.Point(23, 26)
        Me.picThongTinTaiKhoan.Name = "picThongTinTaiKhoan"
        Me.picThongTinTaiKhoan.Size = New System.Drawing.Size(49, 41)
        Me.picThongTinTaiKhoan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picThongTinTaiKhoan.TabIndex = 0
        Me.picThongTinTaiKhoan.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = Global.WindowsApplication1.My.Resources.Resources.phieutra
        Me.PictureBox5.Location = New System.Drawing.Point(434, 47)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(85, 85)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 4
        Me.PictureBox5.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.WindowsApplication1.My.Resources.Resources.phieumuon
        Me.PictureBox4.Location = New System.Drawing.Point(565, 47)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(85, 85)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 3
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.WindowsApplication1.My.Resources.Resources.user
        Me.PictureBox3.Location = New System.Drawing.Point(306, 47)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(78, 85)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.WindowsApplication1.My.Resources.Resources.sac
        Me.PictureBox2.Location = New System.Drawing.Point(166, 47)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(81, 85)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.WindowsApplication1.My.Resources.Resources.cate
        Me.PictureBox1.Location = New System.Drawing.Point(34, 47)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(81, 85)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'btnShortTheoDoiTL
        '
        Me.btnShortTheoDoiTL.Image = Global.WindowsApplication1.My.Resources.Resources.theo_oi
        Me.btnShortTheoDoiTL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortTheoDoiTL.Location = New System.Drawing.Point(6, 94)
        Me.btnShortTheoDoiTL.Name = "btnShortTheoDoiTL"
        Me.btnShortTheoDoiTL.Size = New System.Drawing.Size(192, 33)
        Me.btnShortTheoDoiTL.TabIndex = 2
        Me.btnShortTheoDoiTL.Text = "Theo dõi tài liệu"
        Me.btnShortTheoDoiTL.UseVisualStyleBackColor = True
        '
        'btnShortLapPhieuTra
        '
        Me.btnShortLapPhieuTra.Image = Global.WindowsApplication1.My.Resources.Resources.tra
        Me.btnShortLapPhieuTra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortLapPhieuTra.Location = New System.Drawing.Point(6, 55)
        Me.btnShortLapPhieuTra.Name = "btnShortLapPhieuTra"
        Me.btnShortLapPhieuTra.Size = New System.Drawing.Size(192, 33)
        Me.btnShortLapPhieuTra.TabIndex = 1
        Me.btnShortLapPhieuTra.Text = "Lập phiếu trả"
        Me.btnShortLapPhieuTra.UseVisualStyleBackColor = True
        '
        'btnShortLapPhieuMuon
        '
        Me.btnShortLapPhieuMuon.Image = Global.WindowsApplication1.My.Resources.Resources.muon
        Me.btnShortLapPhieuMuon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortLapPhieuMuon.Location = New System.Drawing.Point(6, 18)
        Me.btnShortLapPhieuMuon.Name = "btnShortLapPhieuMuon"
        Me.btnShortLapPhieuMuon.Size = New System.Drawing.Size(192, 31)
        Me.btnShortLapPhieuMuon.TabIndex = 0
        Me.btnShortLapPhieuMuon.Text = "Lập phiếu mượn"
        Me.btnShortLapPhieuMuon.UseVisualStyleBackColor = True
        '
        'btnShortLoaiDocGia
        '
        Me.btnShortLoaiDocGia.Image = Global.WindowsApplication1.My.Resources.Resources.Cash_register_icon
        Me.btnShortLoaiDocGia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortLoaiDocGia.Location = New System.Drawing.Point(6, 100)
        Me.btnShortLoaiDocGia.Name = "btnShortLoaiDocGia"
        Me.btnShortLoaiDocGia.Size = New System.Drawing.Size(192, 33)
        Me.btnShortLoaiDocGia.TabIndex = 3
        Me.btnShortLoaiDocGia.Text = "Loại độc giả"
        Me.btnShortLoaiDocGia.UseVisualStyleBackColor = True
        '
        'btnShortDangKiDG
        '
        Me.btnShortDangKiDG.Image = Global.WindowsApplication1.My.Resources.Resources.add_doc_gia
        Me.btnShortDangKiDG.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortDangKiDG.Location = New System.Drawing.Point(6, 60)
        Me.btnShortDangKiDG.Name = "btnShortDangKiDG"
        Me.btnShortDangKiDG.Size = New System.Drawing.Size(192, 33)
        Me.btnShortDangKiDG.TabIndex = 1
        Me.btnShortDangKiDG.Text = "Đăng kí độc giả"
        Me.btnShortDangKiDG.UseVisualStyleBackColor = True
        '
        'btnShortTimDG
        '
        Me.btnShortTimDG.Image = Global.WindowsApplication1.My.Resources.Resources.search_icon__1_
        Me.btnShortTimDG.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortTimDG.Location = New System.Drawing.Point(6, 21)
        Me.btnShortTimDG.Name = "btnShortTimDG"
        Me.btnShortTimDG.Size = New System.Drawing.Size(192, 33)
        Me.btnShortTimDG.TabIndex = 0
        Me.btnShortTimDG.Text = "Tìm kiếm độc giả"
        Me.btnShortTimDG.UseVisualStyleBackColor = True
        '
        'btnShortcutTimTaiLieu
        '
        Me.btnShortcutTimTaiLieu.Image = Global.WindowsApplication1.My.Resources.Resources.Search_Images_icon
        Me.btnShortcutTimTaiLieu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortcutTimTaiLieu.Location = New System.Drawing.Point(6, 14)
        Me.btnShortcutTimTaiLieu.Name = "btnShortcutTimTaiLieu"
        Me.btnShortcutTimTaiLieu.Size = New System.Drawing.Size(192, 33)
        Me.btnShortcutTimTaiLieu.TabIndex = 1
        Me.btnShortcutTimTaiLieu.Text = "Tìm kiếm tài liệu"
        Me.btnShortcutTimTaiLieu.UseVisualStyleBackColor = True
        '
        'btnShortSuaTaiLieu
        '
        Me.btnShortSuaTaiLieu.Image = Global.WindowsApplication1.My.Resources.Resources.edit_dôc
        Me.btnShortSuaTaiLieu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortSuaTaiLieu.Location = New System.Drawing.Point(6, 130)
        Me.btnShortSuaTaiLieu.Name = "btnShortSuaTaiLieu"
        Me.btnShortSuaTaiLieu.Size = New System.Drawing.Size(192, 34)
        Me.btnShortSuaTaiLieu.TabIndex = 3
        Me.btnShortSuaTaiLieu.Text = "Sửa tài liệu"
        Me.btnShortSuaTaiLieu.UseVisualStyleBackColor = True
        '
        'btnShortThemTaiLieu
        '
        Me.btnShortThemTaiLieu.Image = Global.WindowsApplication1.My.Resources.Resources.add_bôk
        Me.btnShortThemTaiLieu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortThemTaiLieu.Location = New System.Drawing.Point(6, 92)
        Me.btnShortThemTaiLieu.Name = "btnShortThemTaiLieu"
        Me.btnShortThemTaiLieu.Size = New System.Drawing.Size(192, 35)
        Me.btnShortThemTaiLieu.TabIndex = 2
        Me.btnShortThemTaiLieu.Text = "Thêm tài liệu"
        Me.btnShortThemTaiLieu.UseVisualStyleBackColor = True
        '
        'btnShortThemDanhMuc
        '
        Me.btnShortThemDanhMuc.Image = Global.WindowsApplication1.My.Resources.Resources.add_cat
        Me.btnShortThemDanhMuc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShortThemDanhMuc.Location = New System.Drawing.Point(6, 53)
        Me.btnShortThemDanhMuc.Name = "btnShortThemDanhMuc"
        Me.btnShortThemDanhMuc.Size = New System.Drawing.Size(192, 35)
        Me.btnShortThemDanhMuc.TabIndex = 0
        Me.btnShortThemDanhMuc.Text = "Thêm danh mục"
        Me.btnShortThemDanhMuc.UseVisualStyleBackColor = True
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(32, 20)
        Me.ToolStripSplitButton1.Text = "ToolStripSplitButton1"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(910, 555)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.sttMain)
        Me.Controls.Add(Me.mnuMain)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.mnuMain
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Hệ thống quản lý thư viện  NoName"
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.sttMain.ResumeLayout(False)
        Me.sttMain.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.picDangXuat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picThongTinTaiKhoan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents TaifToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HệThốngToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ĐộcGiảToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuMuonSach As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGioiThieu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents sttMain As System.Windows.Forms.StatusStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents optPhieuMuon As System.Windows.Forms.RadioButton
    Friend WithEvents optDocGia As System.Windows.Forms.RadioButton
    Friend WithEvents optTaiLieu As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTimNhanh As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnTimNhanh As System.Windows.Forms.Button
    Friend WithEvents btnShortThemTaiLieu As System.Windows.Forms.Button
    Friend WithEvents btnShortcutTimTaiLieu As System.Windows.Forms.Button
    Friend WithEvents btnShortThemDanhMuc As System.Windows.Forms.Button
    Friend WithEvents btnShortSuaTaiLieu As System.Windows.Forms.Button
    Friend WithEvents btnShortDangKiDG As System.Windows.Forms.Button
    Friend WithEvents btnShortTimDG As System.Windows.Forms.Button
    Friend WithEvents btnShortTheoDoiTL As System.Windows.Forms.Button
    Friend WithEvents btnShortLapPhieuTra As System.Windows.Forms.Button
    Friend WithEvents btnShortLapPhieuMuon As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents NgườiDùngToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents picThongTinTaiKhoan As System.Windows.Forms.PictureBox
    Friend WithEvents optPhieuTra As System.Windows.Forms.RadioButton
    Friend WithEvents picDangXuat As System.Windows.Forms.PictureBox
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents submnuCaiDat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportDữLiệuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuThoat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuDanhMuc As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuTaiLieu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuThemTaiLieu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuLoaiDG As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuDG As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuThuPhi As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuIn As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MượnSáchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuTraSach As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuPhiPhatSinh As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuTaikhoan As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents submnuThemTaiKhoan As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ĐăngXuấtToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnShortLoaiDocGia As System.Windows.Forms.Button

End Class
