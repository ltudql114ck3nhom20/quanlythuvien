﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDangKiDocGia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDangKiDocGia))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.optDaDong = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optKhac = New System.Windows.Forms.RadioButton()
        Me.optNu = New System.Windows.Forms.RadioButton()
        Me.optNam = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbLoaiDG = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtNgayDangKi = New System.Windows.Forms.DateTimePicker()
        Me.txtNgayDangKy = New System.Windows.Forms.Label()
        Me.txtDiaChi = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtHoTen = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.optChuaDong = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.optDisable = New System.Windows.Forms.RadioButton()
        Me.optEnable = New System.Windows.Forms.RadioButton()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnDangKy = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.txtCMND = New System.Windows.Forms.MaskedTextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtCMND)
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cbLoaiDG)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.dtNgayDangKi)
        Me.GroupBox1.Controls.Add(Me.txtNgayDangKy)
        Me.GroupBox1.Controls.Add(Me.txtDiaChi)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtHoTen)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(397, 292)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thông tin đăng kí"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.optChuaDong)
        Me.GroupBox3.Controls.Add(Me.optDaDong)
        Me.GroupBox3.Controls.Add(Me.RadioButton6)
        Me.GroupBox3.Location = New System.Drawing.Point(107, 219)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(199, 28)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        '
        'optDaDong
        '
        Me.optDaDong.AutoSize = True
        Me.optDaDong.Location = New System.Drawing.Point(116, 7)
        Me.optDaDong.Name = "optDaDong"
        Me.optDaDong.Size = New System.Drawing.Size(67, 17)
        Me.optDaDong.TabIndex = 1
        Me.optDaDong.Text = "Đã đóng"
        Me.optDaDong.UseVisualStyleBackColor = True
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Location = New System.Drawing.Point(6, 36)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(78, 17)
        Me.RadioButton6.TabIndex = 0
        Me.RadioButton6.Text = "Chưa đóng"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 228)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Đóng phí"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optKhac)
        Me.GroupBox2.Controls.Add(Me.optNu)
        Me.GroupBox2.Controls.Add(Me.optNam)
        Me.GroupBox2.Location = New System.Drawing.Point(107, 185)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(199, 27)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        '
        'optKhac
        '
        Me.optKhac.AutoSize = True
        Me.optKhac.Location = New System.Drawing.Point(116, 8)
        Me.optKhac.Name = "optKhac"
        Me.optKhac.Size = New System.Drawing.Size(50, 17)
        Me.optKhac.TabIndex = 2
        Me.optKhac.TabStop = True
        Me.optKhac.Text = "Khác"
        Me.optKhac.UseVisualStyleBackColor = True
        '
        'optNu
        '
        Me.optNu.AutoSize = True
        Me.optNu.Location = New System.Drawing.Point(59, 8)
        Me.optNu.Name = "optNu"
        Me.optNu.Size = New System.Drawing.Size(39, 17)
        Me.optNu.TabIndex = 1
        Me.optNu.TabStop = True
        Me.optNu.Text = "Nữ"
        Me.optNu.UseVisualStyleBackColor = True
        '
        'optNam
        '
        Me.optNam.AutoSize = True
        Me.optNam.Location = New System.Drawing.Point(6, 8)
        Me.optNam.Name = "optNam"
        Me.optNam.Size = New System.Drawing.Size(47, 17)
        Me.optNam.TabIndex = 0
        Me.optNam.TabStop = True
        Me.optNam.Text = "Nam"
        Me.optNam.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 195)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Giới tính"
        '
        'cbLoaiDG
        '
        Me.cbLoaiDG.FormattingEnabled = True
        Me.cbLoaiDG.Location = New System.Drawing.Point(107, 158)
        Me.cbLoaiDG.Name = "cbLoaiDG"
        Me.cbLoaiDG.Size = New System.Drawing.Size(199, 21)
        Me.cbLoaiDG.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 161)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Loại độc giả"
        '
        'dtNgayDangKi
        '
        Me.dtNgayDangKi.CalendarMonthBackground = System.Drawing.SystemColors.Info
        Me.dtNgayDangKi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtNgayDangKi.Location = New System.Drawing.Point(107, 129)
        Me.dtNgayDangKi.Name = "dtNgayDangKi"
        Me.dtNgayDangKi.Size = New System.Drawing.Size(199, 20)
        Me.dtNgayDangKi.TabIndex = 7
        Me.dtNgayDangKi.Value = New Date(2016, 12, 16, 13, 44, 41, 0)
        '
        'txtNgayDangKy
        '
        Me.txtNgayDangKy.AutoSize = True
        Me.txtNgayDangKy.Location = New System.Drawing.Point(6, 132)
        Me.txtNgayDangKy.Name = "txtNgayDangKy"
        Me.txtNgayDangKy.Size = New System.Drawing.Size(74, 13)
        Me.txtNgayDangKy.TabIndex = 6
        Me.txtNgayDangKy.Text = "Ngày Đăng kí"
        '
        'txtDiaChi
        '
        Me.txtDiaChi.Location = New System.Drawing.Point(107, 74)
        Me.txtDiaChi.Multiline = True
        Me.txtDiaChi.Name = "txtDiaChi"
        Me.txtDiaChi.Size = New System.Drawing.Size(284, 49)
        Me.txtDiaChi.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Địa chỉ"
        '
        'txtHoTen
        '
        Me.txtHoTen.Location = New System.Drawing.Point(107, 48)
        Me.txtHoTen.MaxLength = 200
        Me.txtHoTen.Name = "txtHoTen"
        Me.txtHoTen.Size = New System.Drawing.Size(284, 20)
        Me.txtHoTen.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Họ Tên"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "CMND"
        '
        'optChuaDong
        '
        Me.optChuaDong.AutoSize = True
        Me.optChuaDong.Checked = True
        Me.optChuaDong.Location = New System.Drawing.Point(6, 7)
        Me.optChuaDong.Name = "optChuaDong"
        Me.optChuaDong.Size = New System.Drawing.Size(78, 17)
        Me.optChuaDong.TabIndex = 2
        Me.optChuaDong.TabStop = True
        Me.optChuaDong.Text = "Chưa đóng"
        Me.optChuaDong.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.optDisable)
        Me.GroupBox4.Controls.Add(Me.optEnable)
        Me.GroupBox4.Controls.Add(Me.RadioButton9)
        Me.GroupBox4.Location = New System.Drawing.Point(107, 253)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(199, 28)
        Me.GroupBox4.TabIndex = 16
        Me.GroupBox4.TabStop = False
        '
        'optDisable
        '
        Me.optDisable.AutoSize = True
        Me.optDisable.Location = New System.Drawing.Point(6, 7)
        Me.optDisable.Name = "optDisable"
        Me.optDisable.Size = New System.Drawing.Size(60, 17)
        Me.optDisable.TabIndex = 2
        Me.optDisable.Text = "Disable"
        Me.optDisable.UseVisualStyleBackColor = True
        '
        'optEnable
        '
        Me.optEnable.AutoSize = True
        Me.optEnable.Checked = True
        Me.optEnable.Location = New System.Drawing.Point(116, 7)
        Me.optEnable.Name = "optEnable"
        Me.optEnable.Size = New System.Drawing.Size(58, 17)
        Me.optEnable.TabIndex = 1
        Me.optEnable.TabStop = True
        Me.optEnable.Text = "Enable"
        Me.optEnable.UseVisualStyleBackColor = True
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = True
        Me.RadioButton9.Location = New System.Drawing.Point(6, 36)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(78, 17)
        Me.RadioButton9.TabIndex = 0
        Me.RadioButton9.Text = "Chưa đóng"
        Me.RadioButton9.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 262)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Kích hoạt"
        '
        'btnDangKy
        '
        Me.btnDangKy.Location = New System.Drawing.Point(326, 313)
        Me.btnDangKy.Name = "btnDangKy"
        Me.btnDangKy.Size = New System.Drawing.Size(84, 28)
        Me.btnDangKy.TabIndex = 1
        Me.btnDangKy.Text = "Đăng kí"
        Me.btnDangKy.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(234, 313)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(84, 28)
        Me.btnReset.TabIndex = 2
        Me.btnReset.Text = "Làm lại"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'txtCMND
        '
        Me.txtCMND.Location = New System.Drawing.Point(107, 22)
        Me.txtCMND.Mask = "#########"
        Me.txtCMND.Name = "txtCMND"
        Me.txtCMND.Size = New System.Drawing.Size(284, 20)
        Me.txtCMND.TabIndex = 17
        '
        'frmDangKiDocGia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(422, 349)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnDangKy)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmDangKiDocGia"
        Me.Text = "Đăng kí độc giả"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cbLoaiDG As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtNgayDangKi As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNgayDangKy As System.Windows.Forms.Label
    Friend WithEvents txtDiaChi As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtHoTen As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents optDaDong As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optKhac As System.Windows.Forms.RadioButton
    Friend WithEvents optNu As System.Windows.Forms.RadioButton
    Friend WithEvents optNam As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents optDisable As System.Windows.Forms.RadioButton
    Friend WithEvents optEnable As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton9 As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents optChuaDong As System.Windows.Forms.RadioButton
    Friend WithEvents btnDangKy As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents txtCMND As System.Windows.Forms.MaskedTextBox
End Class
