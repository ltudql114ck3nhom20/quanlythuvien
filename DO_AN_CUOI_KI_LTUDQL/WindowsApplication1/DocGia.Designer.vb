﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DocGia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTuKhoa = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optMaDG = New System.Windows.Forms.RadioButton()
        Me.optTenDG = New System.Windows.Forms.RadioButton()
        Me.optCMND = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnTimKiem = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvListDG = New System.Windows.Forms.DataGridView()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnCapNhatDG = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtChiTietMaDG = New System.Windows.Forms.TextBox()
        Me.txtChiTietCMND = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtChiTietHoTenDG = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtChiTietDiaChiDG = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtChiTietNgayDangKy = New System.Windows.Forms.DateTimePicker()
        Me.cbChiTietLoaiDG = New System.Windows.Forms.ComboBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.optChiTietNam = New System.Windows.Forms.RadioButton()
        Me.optChiTietNu = New System.Windows.Forms.RadioButton()
        Me.optChiTietKhac = New System.Windows.Forms.RadioButton()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.optChiTietDaDong = New System.Windows.Forms.RadioButton()
        Me.optChiTietChuaDong = New System.Windows.Forms.RadioButton()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.optChiTietDaKichHoat = New System.Windows.Forms.RadioButton()
        Me.optChiTietChuaKichHoat = New System.Windows.Forms.RadioButton()
        Me.btnIn = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvListDG, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.btnTimKiem)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtTuKhoa)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(637, 136)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tìm độc giả"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tìm kiếm"
        '
        'txtTuKhoa
        '
        Me.txtTuKhoa.Location = New System.Drawing.Point(103, 25)
        Me.txtTuKhoa.Name = "txtTuKhoa"
        Me.txtTuKhoa.Size = New System.Drawing.Size(527, 21)
        Me.txtTuKhoa.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optCMND)
        Me.GroupBox2.Controls.Add(Me.optTenDG)
        Me.GroupBox2.Controls.Add(Me.optMaDG)
        Me.GroupBox2.Location = New System.Drawing.Point(191, 55)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(438, 30)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'optMaDG
        '
        Me.optMaDG.AutoSize = True
        Me.optMaDG.Location = New System.Drawing.Point(7, 8)
        Me.optMaDG.Name = "optMaDG"
        Me.optMaDG.Size = New System.Drawing.Size(86, 19)
        Me.optMaDG.TabIndex = 0
        Me.optMaDG.TabStop = True
        Me.optMaDG.Text = "Mã độc giả"
        Me.optMaDG.UseVisualStyleBackColor = True
        '
        'optTenDG
        '
        Me.optTenDG.AutoSize = True
        Me.optTenDG.Location = New System.Drawing.Point(132, 8)
        Me.optTenDG.Name = "optTenDG"
        Me.optTenDG.Size = New System.Drawing.Size(89, 19)
        Me.optTenDG.TabIndex = 1
        Me.optTenDG.TabStop = True
        Me.optTenDG.Text = "Tên độc giả"
        Me.optTenDG.UseVisualStyleBackColor = True
        '
        'optCMND
        '
        Me.optCMND.AutoSize = True
        Me.optCMND.Location = New System.Drawing.Point(262, 8)
        Me.optCMND.Name = "optCMND"
        Me.optCMND.Size = New System.Drawing.Size(62, 19)
        Me.optCMND.TabIndex = 2
        Me.optCMND.TabStop = True
        Me.optCMND.Text = "CMND"
        Me.optCMND.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(100, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Tìm kiếm theo"
        '
        'btnTimKiem
        '
        Me.btnTimKiem.Location = New System.Drawing.Point(260, 91)
        Me.btnTimKiem.Name = "btnTimKiem"
        Me.btnTimKiem.Size = New System.Drawing.Size(106, 26)
        Me.btnTimKiem.TabIndex = 4
        Me.btnTimKiem.Text = "Tìm kiếm"
        Me.btnTimKiem.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.GroupBox3.Controls.Add(Me.GroupBox7)
        Me.GroupBox3.Controls.Add(Me.GroupBox6)
        Me.GroupBox3.Controls.Add(Me.GroupBox5)
        Me.GroupBox3.Controls.Add(Me.cbChiTietLoaiDG)
        Me.GroupBox3.Controls.Add(Me.dtChiTietNgayDangKy)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.txtChiTietDiaChiDG)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.txtChiTietHoTenDG)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.txtChiTietCMND)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.txtChiTietMaDG)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(14, 377)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(637, 191)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Thông tin chi tiết"
        '
        'dgvListDG
        '
        Me.dgvListDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListDG.Location = New System.Drawing.Point(0, 17)
        Me.dgvListDG.Name = "dgvListDG"
        Me.dgvListDG.Size = New System.Drawing.Size(637, 198)
        Me.dgvListDG.TabIndex = 2
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvListDG)
        Me.GroupBox4.Location = New System.Drawing.Point(14, 156)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(637, 221)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Kết quả"
        '
        'btnCapNhatDG
        '
        Me.btnCapNhatDG.Enabled = False
        Me.btnCapNhatDG.Location = New System.Drawing.Point(572, 574)
        Me.btnCapNhatDG.Name = "btnCapNhatDG"
        Me.btnCapNhatDG.Size = New System.Drawing.Size(79, 28)
        Me.btnCapNhatDG.TabIndex = 5
        Me.btnCapNhatDG.Text = "Cập nhật"
        Me.btnCapNhatDG.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Mã"
        '
        'txtChiTietMaDG
        '
        Me.txtChiTietMaDG.Enabled = False
        Me.txtChiTietMaDG.Location = New System.Drawing.Point(65, 23)
        Me.txtChiTietMaDG.Name = "txtChiTietMaDG"
        Me.txtChiTietMaDG.Size = New System.Drawing.Size(100, 21)
        Me.txtChiTietMaDG.TabIndex = 1
        '
        'txtChiTietCMND
        '
        Me.txtChiTietCMND.Location = New System.Drawing.Point(65, 50)
        Me.txtChiTietCMND.Name = "txtChiTietCMND"
        Me.txtChiTietCMND.Size = New System.Drawing.Size(247, 21)
        Me.txtChiTietCMND.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 15)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "CMND"
        '
        'txtChiTietHoTenDG
        '
        Me.txtChiTietHoTenDG.Location = New System.Drawing.Point(65, 77)
        Me.txtChiTietHoTenDG.Name = "txtChiTietHoTenDG"
        Me.txtChiTietHoTenDG.Size = New System.Drawing.Size(247, 21)
        Me.txtChiTietHoTenDG.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Họ tên"
        '
        'txtChiTietDiaChiDG
        '
        Me.txtChiTietDiaChiDG.Location = New System.Drawing.Point(65, 104)
        Me.txtChiTietDiaChiDG.Multiline = True
        Me.txtChiTietDiaChiDG.Name = "txtChiTietDiaChiDG"
        Me.txtChiTietDiaChiDG.Size = New System.Drawing.Size(247, 81)
        Me.txtChiTietDiaChiDG.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 107)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 15)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Địa chỉ"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(341, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 15)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Ngày đăng kí"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(341, 53)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(74, 15)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Loại độc giả"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(341, 89)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 15)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Giới tính"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(341, 122)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 15)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Đóng phí"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(341, 158)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 15)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Kích hoạt"
        '
        'dtChiTietNgayDangKy
        '
        Me.dtChiTietNgayDangKy.Location = New System.Drawing.Point(425, 21)
        Me.dtChiTietNgayDangKy.Name = "dtChiTietNgayDangKy"
        Me.dtChiTietNgayDangKy.Size = New System.Drawing.Size(204, 21)
        Me.dtChiTietNgayDangKy.TabIndex = 13
        '
        'cbChiTietLoaiDG
        '
        Me.cbChiTietLoaiDG.FormattingEnabled = True
        Me.cbChiTietLoaiDG.Location = New System.Drawing.Point(425, 50)
        Me.cbChiTietLoaiDG.Name = "cbChiTietLoaiDG"
        Me.cbChiTietLoaiDG.Size = New System.Drawing.Size(204, 23)
        Me.cbChiTietLoaiDG.TabIndex = 14
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.optChiTietKhac)
        Me.GroupBox5.Controls.Add(Me.optChiTietNu)
        Me.GroupBox5.Controls.Add(Me.optChiTietNam)
        Me.GroupBox5.Location = New System.Drawing.Point(425, 77)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(200, 35)
        Me.GroupBox5.TabIndex = 15
        Me.GroupBox5.TabStop = False
        '
        'optChiTietNam
        '
        Me.optChiTietNam.AutoSize = True
        Me.optChiTietNam.Location = New System.Drawing.Point(4, 11)
        Me.optChiTietNam.Name = "optChiTietNam"
        Me.optChiTietNam.Size = New System.Drawing.Size(52, 19)
        Me.optChiTietNam.TabIndex = 0
        Me.optChiTietNam.TabStop = True
        Me.optChiTietNam.Text = "Nam"
        Me.optChiTietNam.UseVisualStyleBackColor = True
        '
        'optChiTietNu
        '
        Me.optChiTietNu.AutoSize = True
        Me.optChiTietNu.Location = New System.Drawing.Point(62, 11)
        Me.optChiTietNu.Name = "optChiTietNu"
        Me.optChiTietNu.Size = New System.Drawing.Size(41, 19)
        Me.optChiTietNu.TabIndex = 1
        Me.optChiTietNu.TabStop = True
        Me.optChiTietNu.Text = "Nữ"
        Me.optChiTietNu.UseVisualStyleBackColor = True
        '
        'optChiTietKhac
        '
        Me.optChiTietKhac.AutoSize = True
        Me.optChiTietKhac.Location = New System.Drawing.Point(120, 11)
        Me.optChiTietKhac.Name = "optChiTietKhac"
        Me.optChiTietKhac.Size = New System.Drawing.Size(53, 19)
        Me.optChiTietKhac.TabIndex = 16
        Me.optChiTietKhac.TabStop = True
        Me.optChiTietKhac.Text = "Khác"
        Me.optChiTietKhac.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.optChiTietDaDong)
        Me.GroupBox6.Controls.Add(Me.optChiTietChuaDong)
        Me.GroupBox6.Location = New System.Drawing.Point(425, 112)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(200, 32)
        Me.GroupBox6.TabIndex = 17
        Me.GroupBox6.TabStop = False
        '
        'optChiTietDaDong
        '
        Me.optChiTietDaDong.AutoSize = True
        Me.optChiTietDaDong.Location = New System.Drawing.Point(120, 11)
        Me.optChiTietDaDong.Name = "optChiTietDaDong"
        Me.optChiTietDaDong.Size = New System.Drawing.Size(72, 19)
        Me.optChiTietDaDong.TabIndex = 1
        Me.optChiTietDaDong.TabStop = True
        Me.optChiTietDaDong.Text = "Đã đóng"
        Me.optChiTietDaDong.UseVisualStyleBackColor = True
        '
        'optChiTietChuaDong
        '
        Me.optChiTietChuaDong.AutoSize = True
        Me.optChiTietChuaDong.Location = New System.Drawing.Point(4, 10)
        Me.optChiTietChuaDong.Name = "optChiTietChuaDong"
        Me.optChiTietChuaDong.Size = New System.Drawing.Size(85, 19)
        Me.optChiTietChuaDong.TabIndex = 0
        Me.optChiTietChuaDong.TabStop = True
        Me.optChiTietChuaDong.Text = "Chưa đóng"
        Me.optChiTietChuaDong.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.optChiTietChuaKichHoat)
        Me.GroupBox7.Controls.Add(Me.optChiTietDaKichHoat)
        Me.GroupBox7.Location = New System.Drawing.Point(425, 148)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(200, 32)
        Me.GroupBox7.TabIndex = 18
        Me.GroupBox7.TabStop = False
        '
        'optChiTietDaKichHoat
        '
        Me.optChiTietDaKichHoat.AutoSize = True
        Me.optChiTietDaKichHoat.Location = New System.Drawing.Point(4, 9)
        Me.optChiTietDaKichHoat.Name = "optChiTietDaKichHoat"
        Me.optChiTietDaKichHoat.Size = New System.Drawing.Size(40, 19)
        Me.optChiTietDaKichHoat.TabIndex = 0
        Me.optChiTietDaKichHoat.TabStop = True
        Me.optChiTietDaKichHoat.Text = "Có"
        Me.optChiTietDaKichHoat.UseVisualStyleBackColor = True
        '
        'optChiTietChuaKichHoat
        '
        Me.optChiTietChuaKichHoat.AutoSize = True
        Me.optChiTietChuaKichHoat.Location = New System.Drawing.Point(62, 9)
        Me.optChiTietChuaKichHoat.Name = "optChiTietChuaKichHoat"
        Me.optChiTietChuaKichHoat.Size = New System.Drawing.Size(61, 19)
        Me.optChiTietChuaKichHoat.TabIndex = 1
        Me.optChiTietChuaKichHoat.TabStop = True
        Me.optChiTietChuaKichHoat.Text = "Không"
        Me.optChiTietChuaKichHoat.UseVisualStyleBackColor = True
        '
        'btnIn
        '
        Me.btnIn.Enabled = False
        Me.btnIn.Location = New System.Drawing.Point(483, 574)
        Me.btnIn.Name = "btnIn"
        Me.btnIn.Size = New System.Drawing.Size(79, 28)
        Me.btnIn.TabIndex = 6
        Me.btnIn.Text = "In thông tin"
        Me.btnIn.UseVisualStyleBackColor = True
        '
        'DocGia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(663, 614)
        Me.Controls.Add(Me.btnIn)
        Me.Controls.Add(Me.btnCapNhatDG)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.Name = "DocGia"
        Me.Text = "Quản lý độc giả"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvListDG, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnTimKiem As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optCMND As System.Windows.Forms.RadioButton
    Friend WithEvents optTenDG As System.Windows.Forms.RadioButton
    Friend WithEvents optMaDG As System.Windows.Forms.RadioButton
    Friend WithEvents txtTuKhoa As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvListDG As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCapNhatDG As System.Windows.Forms.Button
    Friend WithEvents txtChiTietCMND As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtChiTietMaDG As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtChiTietHoTenDG As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtChiTietDiaChiDG As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cbChiTietLoaiDG As System.Windows.Forms.ComboBox
    Friend WithEvents dtChiTietNgayDangKy As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents optChiTietDaDong As System.Windows.Forms.RadioButton
    Friend WithEvents optChiTietChuaDong As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents optChiTietKhac As System.Windows.Forms.RadioButton
    Friend WithEvents optChiTietNu As System.Windows.Forms.RadioButton
    Friend WithEvents optChiTietNam As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents optChiTietChuaKichHoat As System.Windows.Forms.RadioButton
    Friend WithEvents optChiTietDaKichHoat As System.Windows.Forms.RadioButton
    Friend WithEvents btnIn As System.Windows.Forms.Button
End Class
